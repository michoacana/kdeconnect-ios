//
//  PlaceHolderView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-06-18.
//

import SwiftUI

struct PlaceHolderView: View {
    var body: some View {
        Text("Hello, this is a placeholder view")
    }
}

struct PlaceHolderView_Previews: PreviewProvider {
    static var previews: some View {
        PlaceHolderView()
    }
}

